#!/usr/bin/env groovy

@Grapes(
    @Grab(group='net.sf.jt400', module='jt400', version='9.4')
)
import com.ibm.as400.access.AS400
import com.ibm.as400.access.CommandCall

def call(String command, String user, String pass) {
  echo "Command: ${command}."
  AS400 system = new AS400('croas01',"${user}","${pass}");
  cmd = new CommandCall(system)
  if (!cmd.run ("${command}")) {
   for(item in cmd.messageList){
     println item
   } 
   error("Error run command: ${command}.")
 }
}
